package service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ServiceTest {

    private Service s;
    
    Patient p1;
    Patient p2;
    Patient p3;
    Patient p4;
    Patient p5;
    
    Laegemiddel l1;
    Laegemiddel l2;
    Laegemiddel l3;
    Laegemiddel l4;
    
    PN pno1;
    PN pno2;
    PN pno3;
    PN pno4;
    PN pno5;
    
    DagligFast dfo1;
    DagligFast dfo2;
    DagligFast dfo3;
    DagligFast dfo4;
    
    DagligSkaev dso1;
    DagligSkaev dso2;
    DagligSkaev dso3;

    @Before
    public void setUp() throws Exception {
        s = Service.getTestService();
        p1 = s.opretPatient("121256-0512", "Jane Jensen", 15);
        p2 = s.opretPatient("070985-1153", "Finn Madsen", 64);
        p3 = s.opretPatient("050972-1233", "Hans Jørgensen", 145);
        p4 = s.opretPatient("011064-1522", "Ulla Nielsen", 25);
        p5 = s.opretPatient("090149-2529", "Ib Hansen", 120);

        l1 = s.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        l2 = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        l3 = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        l4 = s.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        pno1 = s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20),
            p1, l1,
            2);
        s.ordinationPNAnvendt(pno1, LocalDate.of(2017, 2, 26));
        s.ordinationPNAnvendt(pno1, LocalDate.of(2017, 2, 26));

        pno2 =
            s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20), p2, l4, 2);
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 20));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 20));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 21));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 21));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 22));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 27));

        pno3 =
            s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20), p3, l2, 2);
        s.ordinationPNAnvendt(pno3, LocalDate.of(2017, 2, 20));
        pno4 =
            s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20), p4, l2, 2);
        pno5 = s.opretPNOrdination(LocalDate.of(2017, 2, 26), LocalDate.of(2017, 2, 28), p4, l1, 1);

        dfo1 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p3,
            l4, 3, 2, 1, 0);
        dfo2 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 22),
            LocalDate.of(2017, 2, 28), p2,
            l2, 3, 0, 0, 0);
        dfo3 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p4,
            l1, 0, 0, 0, 0);
        dfo4 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 12),
            LocalDate.of(2017, 2, 28), p2,
            l2, 1, 2, 1, 2);

        LocalTime[] kl1 = { LocalTime.of(9, 30), LocalTime.of(12, 0),
            LocalTime.of(19, 0) };
        double[] an1 = { 3, 2, 1 };

        dso1 = s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p3,
            l2, kl1, an1);

        LocalTime[] kl2 = { LocalTime.of(8, 0) };
        double[] an2 = { 1 };

        dso2 = s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 22),
            LocalDate.of(2017, 2, 28), p2,
            l3, kl2, an2);
        
        LocalTime[] kl3 = {};
        double[] an3 = {};

        dso3 = s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p1,
            l1, kl3, an3);
    }

    @Test
    public void opretPNOrdinationTC1() {
        try {
            s.opretPNOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 26), p5, l1, 1);
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }
    
    @Test
    public void opretPNOrdinationTC2() {
        assertNotNull(
            s.opretPNOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 28), p5, l1, 1));
        assertEquals(1, p5.getOrdinationer().size());
        assertEquals(l1, p5.getOrdinationer().get(0).getLaegemiddel());
    }

    @Test
    public void opretPNOrdinationTC3() {
        try {
            s.opretPNOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 28), p5, l1, -1);
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void opretDagligFastOrdinationTC1() {
        try {
            s.opretDagligFastOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 26), p5,
                l4, 1, 1, 1, 1);
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }
    
    @Test
    public void opretDagligFastOrdinationTC2() {
        assertNotNull(
            s.opretDagligFastOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 28), p5,
                l4, 1, 1, 1, 1));
        assertEquals(1, p5.getOrdinationer().size());
        assertEquals(l4, p5.getOrdinationer().get(0).getLaegemiddel());
    }

    @Test
    public void opretDagligFastOrdinationTC3() {
        try {
            s.opretDagligFastOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 28), p5,
                l4, -1, 1, 1, 1);
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void opretDagligSkaevOrdinationTC1() {
        try {
            LocalTime[] kl1 = { LocalTime.of(9, 30) };
            double[] an1 = { 1 };
            s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 26), p5,
                l1, kl1, an1);
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void opretDagligSkaevOrdinationTC2() {
        LocalTime[] kl1 = { LocalTime.of(9, 30) };
        double[] an1 = { 1 };
        assertNotNull(
            s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 28), p5,
                l1, kl1, an1));
        assertEquals(1, p5.getOrdinationer().size());
        assertEquals(l1, p5.getOrdinationer().get(0).getLaegemiddel());
    }

    @Test
    public void opretDagligSkaevOrdinationTC3() {
        try {
            LocalTime[] kl1 = { LocalTime.of(9, 30) };
            double[] an1 = { 1, 2 };
            s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 27), LocalDate.of(2017, 2, 28), p5,
                l1, kl1, an1);
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void ordinationPNAnvendtTC1() {
        try {
            s.ordinationPNAnvendt(pno5, LocalDate.of(2017, 2, 25));
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void ordinationPNAnvendtTC2() {
        s.ordinationPNAnvendt(pno5, LocalDate.of(2017, 2, 27));
        assertEquals(1, pno5.getAntalGangeGivet());
        assertEquals(LocalDate.of(2017, 2, 27), pno5.getGemteDatoer().get(0));
    }

    @Test
    public void ordinationPNAnvendtTC3() {
        s.ordinationPNAnvendt(pno5, LocalDate.of(2017, 2, 26));
        assertEquals(1, pno5.getAntalGangeGivet());
        assertEquals(LocalDate.of(2017, 2, 26), pno5.getGemteDatoer().get(0));
    }

    @Test
    public void ordinationPNAnvendtTC4() {
        s.ordinationPNAnvendt(pno5, LocalDate.of(2017, 2, 28));
        assertEquals(1, pno5.getAntalGangeGivet());
        assertEquals(LocalDate.of(2017, 2, 28), pno5.getGemteDatoer().get(0));
    }
    
    @Test
    public void anbefaletDosisPrDoegnTC1() {
        assertEquals(1.5, s.anbefaletDosisPrDoegn(p1, l1), 0.02);
    }

    @Test
    public void anbefaletDosisPrDoegnTC2() {
        assertEquals(96, s.anbefaletDosisPrDoegn(p2, l2), 0.02);
    }
    
    @Test
    public void anbefaletDosisPrDoegnTC3() {
        assertEquals(23.2, s.anbefaletDosisPrDoegn(p3, l1), 0.02);
    }

    @Test
    public void anbefaletDosisPrDoegnTC4() {
        assertEquals(37.5, s.anbefaletDosisPrDoegn(p4, l2), 0.02);
    }
    
    @Test
    public void anbefaletDosisPrDoegnTC5() {
        assertEquals(1.8, s.anbefaletDosisPrDoegn(p5, l4), 0.02);
    }
    
    @Test
    public void antalOrdinationerPrVægtPrLægemiddelTC1() {
        assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(60, 70, l4));
    }

    @Test
    public void antalOrdinationerPrVægtPrLægemiddelTC2() {
        assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(1, 10, l2));
    }
    
    @Test
    public void antalOrdinationerPrVægtPrLægemiddelTC3() {
        try {
            s.antalOrdinationerPrVægtPrLægemiddel(-10, 70, l2);
            fail();
        }
        catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void getAllPatienterTC1() {
        assertEquals(5, s.getAllPatienter().size());
    }

    @Test
    public void getAllLaegemidlerTC1() {
        assertEquals(4, s.getAllLaegemidler().size());
    }
}
