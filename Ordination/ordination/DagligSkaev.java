package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    
    private ArrayList<Dosis> doser;
    
    public DagligSkaev(LocalDate startDen,
        LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
        LocalTime[] klokkeSlet, double[] antalEnheder) {
        super(startDen, slutDen, laegemiddel, patient);
        doser = new ArrayList<>();
        for (int i = 0; i < klokkeSlet.length; i++) {
            opretDosis(klokkeSlet[i], antalEnheder[i]);
        }
    }

    public void opretDosis(LocalTime tid, double antal) {
        doser.add(new Dosis(tid, antal));
    }

    @Override
    public double samletDosis() {
        double samletDosis = 0;
        for (Dosis d : doser) {
            samletDosis += d.getAntal();
        }
        return samletDosis * super.antalDage();
    }

    @Override
    public double doegnDosis() {
        return samletDosis() / super.antalDage();
    }

    @Override
    public String getType() {
        return "DagligSkaev";
    }

    public ArrayList<Dosis> getDoser() {
        return new ArrayList<>(doser);
    }

}
