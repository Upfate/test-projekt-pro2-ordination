package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import service.Service;

public class PNTest {
    
    private Service s;
    
    Patient p1;
    Patient p2;
    Patient p3;
    Patient p4;
    Patient p5;
    
    Laegemiddel l1;
    Laegemiddel l2;
    Laegemiddel l3;
    Laegemiddel l4;
    
    PN pno1;
    PN pno2;
    PN pno3;
    PN pno4;
    
    @Before
    public void setUp() throws Exception {        
        s = Service.getTestService();
        p1 = s.opretPatient("121256-0512", "Jane Jensen", 63.4);
        p2 = s.opretPatient("070985-1153", "Finn Madsen", 83.2);
        p3 = s.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        p4 = s.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        p5 = s.opretPatient("090149-2529", "Ib Hansen", 87.7);

        l1 = s.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        l2 = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        l3 = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        l4 = s.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        pno1 = s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20),
            p1, l1,
            2);
        s.ordinationPNAnvendt(pno1, LocalDate.of(2017, 2, 26));
        s.ordinationPNAnvendt(pno1, LocalDate.of(2017, 2, 26));

        pno2 =
            s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20), p2, l4, 2);
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 20));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 20));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 21));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 21));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 22));
        s.ordinationPNAnvendt(pno2, LocalDate.of(2017, 2, 27));

        pno3 =
            s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20), p3, l2, 2);
        s.ordinationPNAnvendt(pno3, LocalDate.of(2017, 2, 20));
        pno4 =
            s.opretPNOrdination(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20), p4, l3, 2);
    }
    
    @Test
    public void testDoegnDosis() {
        assertEquals(pno2.doegnDosis(), 1.5, 0.0001);
        assertEquals(pno3.doegnDosis(), 2, 0.0001);
        assertEquals(pno4.doegnDosis(), 0, 0.0001);
    }
    
    @Test
    public void testSamletDosis() {
        assertEquals(pno2.samletDosis(), 12, 0.0001);
        assertEquals(pno3.samletDosis(), 2, 0.0001);
        assertEquals(pno4.samletDosis(), 0, 0.0001);
    }
    
    @Test
    public void testGivDosis() {
        assertEquals(pno2.givDosis(LocalDate.of(2017, 02, 26)), true);
        assertEquals(pno3.givDosis(LocalDate.now()), true);
        assertEquals(pno4.givDosis(LocalDate.of(2018, 02, 28)), false);
    }
    
}
