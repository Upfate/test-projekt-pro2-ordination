package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
    
    private Dosis[] doser;

    /**
     * Opretter en ordination af typen 'DagligFast'
     * @param startDen datoen for ordinationens start
     * @param slutDen datoen for ordinationens slut
     * @param laegemiddel det laegemiddel som ordineres.
     *
     * Angiver hvor mange enheder der gives i hver af de fire daglige doser.
     * @param morgen
     * @param middag
     * @param aften
     * @param nat
     */
    public DagligFast(LocalDate startDen,
        LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
        double morgenAntal, double middagAntal, double aftenAntal,
        double natAntal) {
        super(startDen, slutDen, laegemiddel, patient);
        doser = new Dosis[4];
        doser[0] = new Dosis(LocalTime.of(6, 00), morgenAntal);
        doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
        doser[2] = new Dosis(LocalTime.of(17, 30), aftenAntal);
        doser[3] = new Dosis(LocalTime.of(23, 00), natAntal);
    }
    
    @Override
    public double samletDosis() {
        double samletDosis = 0;
        for (Dosis d : doser) {
            samletDosis += d.getAntal();
        }
        return samletDosis * super.antalDage();
    }

    @Override
    public double doegnDosis() {
        return samletDosis() / super.antalDage();
    }

    @Override
    public String getType() {
        return "DagligFast";
    }

    public Dosis[] getDoser() {
        Dosis[] temp = new Dosis[doser.length];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = doser[i];
        }
        return temp;
    }
    // TODO
}
