package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
    
    private double antalEnheder;
    private int antalAnvendt;
    private ArrayList<LocalDate> gemteDatoer;
    
    public PN(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
        double antalEnheder) {
        super(startDen, slutDen, laegemiddel, patient);
        this.antalEnheder = antalEnheder;
        this.gemteDatoer = new ArrayList<>();
    }
    
    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
        boolean dos = false;
        if (givesDen.isAfter(getStartDen().minusDays(1))
            && givesDen.isBefore(getSlutDen().plusDays(1))) {
            antalAnvendt++;
            dos = true;
            gemteDatoer.add(givesDen);
        }
        return dos;
    }

    public ArrayList<LocalDate> getGemteDatoer() {
        return new ArrayList<>(gemteDatoer);
    }

    @Override
    public double doegnDosis() {
        long days = 0;
        double doegndosis = 0;
        if (gemteDatoer.size() != 0) {
            days =
                ChronoUnit.DAYS.between(gemteDatoer.get(0), gemteDatoer.get(gemteDatoer.size() - 1))// begge dage inkl.
                    + 1;
            doegndosis = (antalAnvendt * antalEnheder) / days;
        }

        return doegndosis;

    }
    
    @Override
    public double samletDosis() {
        return antalAnvendt * antalEnheder;
    }
    
    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
        return antalAnvendt;
    }
    
    public double getAntalEnheder() {
        return antalEnheder;
    }
    
    @Override
    public String getType() {
        return "PN";
    }
    
}
