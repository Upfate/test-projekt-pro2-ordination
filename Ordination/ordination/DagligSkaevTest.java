package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import service.Service;

public class DagligSkaevTest {
      
    private Service s;
    
    Patient p1;
    Patient p2;
    Patient p3;
    Patient p4;
    Patient p5;
    
    Laegemiddel l1;
    Laegemiddel l2;
    Laegemiddel l3;
    Laegemiddel l4;
    
    DagligSkaev dso1;
    DagligSkaev dso2;
    DagligSkaev dso3;
    
    @Before
    public void setUp() throws Exception {        
        s = Service.getTestService();
        p1 = s.opretPatient("121256-0512", "Jane Jensen", 63.4);
        p2 = s.opretPatient("070985-1153", "Finn Madsen", 83.2);
        p3 = s.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        p4 = s.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        p5 = s.opretPatient("090149-2529", "Ib Hansen", 87.7);

        l1 = s.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        l2 = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        l3 = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        l4 = s.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        LocalTime[] kl1 = { LocalTime.of(9, 30), LocalTime.of(12, 0),
            LocalTime.of(19, 0) };
        double[] an1 = { 3, 2, 1 };

        dso1 = s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p3,
            l2, kl1, an1);

        LocalTime[] kl2 = { LocalTime.of(8, 0) };
        double[] an2 = { 1 };

        dso2 = s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 22),
            LocalDate.of(2017, 2, 28), p2,
            l4, kl2, an2);
        
        LocalTime[] kl3 = {};
        double[] an3 = {};

        dso3 = s.opretDagligSkaevOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p1,
            l1, kl3, an3);
    }
    
    @Test
    public void testSamletDosis() {
        assertEquals(dso1.samletDosis(), 48, 0.0001);
        assertEquals(dso2.samletDosis(), 7, 0.0001);
        assertEquals(dso3.samletDosis(), 0, 0.0001);
    }
    
    @Test
    public void testDoegnDosis() {
        assertEquals(dso1.doegnDosis(), 6, 0.0001);
        assertEquals(dso2.doegnDosis(), 1, 0.0001);
        assertEquals(dso3.doegnDosis(), 0, 0.0001);
    }
    
}
