package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class DagligFastTest {

    private Service s;

    Patient p1;
    Patient p2;
    Patient p3;
    Patient p4;
    Patient p5;

    Laegemiddel l1;
    Laegemiddel l2;
    Laegemiddel l3;
    Laegemiddel l4;
    
    DagligFast dfo1;
    DagligFast dfo2;
    DagligFast dfo3;
    DagligFast dfo4;
    
    @Before
    public void setUp() throws Exception {
        s = Service.getTestService();
        p1 = s.opretPatient("121256-0512", "Jane Jensen", 63.4);
        p2 = s.opretPatient("070985-1153", "Finn Madsen", 83.2);
        p3 = s.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        p4 = s.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        p5 = s.opretPatient("090149-2529", "Ib Hansen", 87.7);
        
        l1 = s.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        l2 = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        l3 = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        l4 = s.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");
        
        dfo1 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p3,
            l4, 3, 2, 1, 0);
        dfo2 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 22),
            LocalDate.of(2017, 2, 28), p2,
            l2, 3, 0, 0, 0);
        dfo3 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 20),
            LocalDate.of(2017, 2, 27), p4,
            l1, 0, 0, 0, 0);
        dfo4 = s.opretDagligFastOrdination(LocalDate.of(2017, 2, 12),
            LocalDate.of(2017, 2, 28), p2,
            l2, 1, 2, 1, 2);
        
    }

    @Test
    public void testSamletDosis() {
        assertEquals(dfo1.samletDosis(), 48, 0.001);
        assertEquals(dfo2.samletDosis(), 21, 0.001);
        assertEquals(dfo3.samletDosis(), 0, 0.001);
        
    }
    
    @Test
    public void testDoegnDosis() {
        assertEquals(dfo1.doegnDosis(), 6, 0.001);
        assertEquals(dfo2.doegnDosis(), 3, 0.001);
        assertEquals(dfo3.doegnDosis(), 0, 0.001);
    }
    
}
