package ordination;

import java.util.ArrayList;

public class Patient {
    private String cprnr;
    private String navn;
    private double vaegt;
    private ArrayList<Ordination> ordinationer;
    
    public Patient(String cprnr, String navn, double vaegt) {
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
        ordinationer = new ArrayList<>();
    }
    
    public String getCprnr() {
        return cprnr;
    }
    
    public String getNavn() {
        return navn;
    }
    
    public void setNavn(String navn) {
        this.navn = navn;
    }
    
    public double getVaegt() {
        return vaegt;
    }
    
    public void setVaegt(double vaegt) {
        this.vaegt = vaegt;
    }
    
    @Override
    public String toString() {
        return navn + "  " + cprnr;
    }

    /**
     * Returener en liste over de ordinationer patienten har
     * @return
     */

    public ArrayList<Ordination> getOrdinationer() {
        return new ArrayList<>(ordinationer);
    }
    
    /**
     * Tillægger ordinationen til denne patients ordinationsliste
     * Pre: patienten har ikke ordination på samme lægemiddel eller
     * synonympræparat i forvejen
     * @return
     */
    public void addOrdination(Ordination ordination) {
        ordinationer.add(ordination);
    }

}
